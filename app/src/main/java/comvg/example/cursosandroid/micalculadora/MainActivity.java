package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText txtNum1;
    private EditText txtNum2;
    private EditText txtResul;
    private Button btnSumar,btnRestar,btnMultiplicar,btnDividir;
    private Button btnLimpiar,btnCerrar;
    private Operaciones op = new Operaciones(0.0f,0.0f);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initComponents();
    }
    public void initComponents(){
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResul = (EditText) findViewById(R.id.txtRes);
        txtResul.setEnabled(false);

        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMultiplicar = (Button) findViewById(R.id.btnMult);
        btnDividir = (Button) findViewById(R.id.btnDivi);

        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        this.setEvents();

    }

    public void setEvents(){
        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnMultiplicar.setOnClickListener(this);
        this.btnDividir.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        //Realizar Operaciones
        switch(view.getId()){
            case R.id.btnSuma://Sumar
                op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                float suma = op.suma();
                txtResul.setText(String.valueOf(suma));
                break;
            case R.id.btnResta:
                op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                float resta = op.resta();
                txtResul.setText(String.valueOf(resta));
                break;
            case R.id.btnMult:
                op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                float multi = op.mult();
                txtResul.setText(String.valueOf(multi));
                break;
            case R.id.btnDivi:
                op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                float divi = op.div();
                txtResul.setText(String.valueOf(divi));
                break;
            case R.id.btnLimpiar:
                Limpiar();
                break;
            case R.id.btnCerrar:
                finishAndRemoveTask();
                break;
        }
    }
    public void Limpiar(){
        txtNum1.setText("");
        txtNum2.setText("");
        txtResul.setText("");
        txtNum1.requestFocus();
    }
}
